var url = require('url');

exports.get = function(req, res) {
    req.requrl = url.parse(req.url, true);
    var path = req.requrl.pathname;

    // handle routes
    if (path === '/' || path === '/home') {
      require('./controllers/home').get(req, res);
    } else {
      require('./controllers/404').get(req, res);
    }
}
