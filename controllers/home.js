var template = require('../views/template-main');
exports.get = function(req, res) {
    res.writeHead(200, {'Content-Type':'text/html'});
    res.write(template.build("cjsoft - home","cjsoft","<p>Here at cjsoft we pride ourselves on software quality.</p>"));
    res.end();
};
