var http = require('http');

var server = http.createServer(function (req, res) {
    require('./router').get(req, res);
}).listen(8080);

console.log("Server running at http://127.0.0.1:8080/");
