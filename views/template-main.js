exports.build = function(title,pagetitle,content) {
    return ['<!doctype html>',
            '<html lang="en"><meta charset="utf-8"><title>{title}</title>',
            '<head>',
            '    <link rel="stylesheet" href="/assets/style.css" />',
            '</head>',
            '<body>',
            '    <h1>{pagetitle}</h1>',
            '    {content}',
            '</body>',
            '</html>'].join('\n').replace(/{title}/g, title).replace(/{pagetitle}/g, pagetitle).replace(/{content}/g, content);
};
